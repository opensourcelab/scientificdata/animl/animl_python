AnIML_python TODO list
======================

- Technique
- 

Ideas
------

- sample AnIML file generator:
  gen_random_chromatogram(500, type='HPLC', det_method='UVvis', ...) 


Analytical Techniques
------------------------



## AnIML Techniques

For these standard techniques Technique definitions are missing:

Here is a (incomplete) list of techniques in a standard laboratory, where a AnIML Technique definition is missing (yes there is a lot of work coming for AniML):

(Many of these techniques, we have e.g. in the lab or at least in the institute).


* Mass spectroscopy

  - 1D, 2D, ....

   - MALDI (matrix, parameters, )

* NMR

  - nD (not only 1D)

* ESR / EPR

* UV-Vis spectroscopy

  - !! similarity to other methods: UV-vis in HPLC detector

* Fluorimetry

* Luminometry

* IR / FT-IR

* Raman spectroscopy

* Diffractometry

* Polariometry

* rtPCR

* nextGen Sequencing (many flavours, like IonTorrent, Illumina, nanoPore)...

* PAGE

* Argarose Gels

* TLC


* Plate Readers

  - UV-vis

  - fluorescence

  - luminescence

* (flow) cytometry

* capillary electrophoresis (CE)

* affinity / size exclusion / ion exchange chromatography

* dynamic light scattering

* Calorimetry (DSC , ...)

* Circular dicrosim spectroscopy (CD)

* DSF

* melting point analysis

* Elemental Analysis (EA)


* liquid handling robot protocols (?)

* many micoscopy techniques (light, TEM, SEM, ....)

* X-ray diffractometry (XRD, ...)

* X-ray scattering

* many other X-ray techniques


* simple physical measurements: temperature, mass, voltage, current, resistance, pressure, light intensity,  viscosity,


* Incubation monitoring / temperature control

* chemical reaction monitoring (or more general: monitoring of bio-chemical processes)

