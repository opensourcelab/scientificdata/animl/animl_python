AnIML converter
================

These modules convert AnIML documents to other formats and vice versa.

Examples:

- AnIML <-> SciData
- AnIML <-> RDF

