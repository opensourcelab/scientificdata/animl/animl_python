"""_____________________________________________________________________

:PROJECT: AnIML_python

*animl - .*

:details: AnIML.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200407

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging
from lxml import objectify, etree

class AnIMLCreator:
    """
      AnIML class
    """
    def __init__(self, namespace: str = "urn:org:astm:animl:schema:core:1.0"):

      # etree.register_namespace("animl", namespace)
      self.alE = objectify.ElementMaker(annotate=False,
                       namespace=namespace, nsmap={None : namespace})

      self.root = None
      self.samples = []
      self.experiment_steps = []
      self.add_audittrail_entries = []

    def add_sample(self):
      SAMPLE = self.alE.Sample
      self.samples.append(SAMPLE())

    def add_eperiment_step(self, parameter_list):
      pass

    def add_audittrail_entry(self, parameter_list):
      pass

    def add_signature(self, parameter_list):
      pass

    def compile(self) -> str:

      # lxml E-factory definitions
      ROOT = self.alE.AnIML

      SAMPLESET = self.alE.SampleSet

      EXPERIMENTSTEPSET = self.alE.ExperimentStepSet
      EXPERIMENTSTEP = self.alE.ExperimentStep

      AUDITTRAILENTRYSET = self.alE.AuditTrailEntrySet
      AUDITTRAILENTRY = self.alE.AuditTrailEntry

      SIGNATURESET = self.alE.SignatureSet
      SIGNATURE = self.alE.Signature

      # actual tree generation
      self.root = ROOT(
                  SAMPLESET( *self.samples ),
                  EXPERIMENTSTEPSET( EXPERIMENTSTEP () ),
                  AUDITTRAILENTRYSET( AUDITTRAILENTRY() ),
                  SIGNATURESET( SIGNATURE()),
                )
      return str(self)

    def validate(self, parameter_list) -> bool:
      return True

    def __str__(self) -> str:
      return etree.tostring(self.root, pretty_print=True, 
              encoding="UTF-8", 
              xml_declaration=True, doctype="xml").decode("UTF-8")

