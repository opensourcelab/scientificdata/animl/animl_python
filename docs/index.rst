.. pythonLab documentation master file, created by
   sphinx-quickstart on Sat Mar 14 18:02:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pythonLab's documentation!
=====================================

Project Version
----------------
.. include:: ../VERSION


Specification
---------------






Quickstart
-----------

This is a quickstart quide to run the implementation ...


.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:
   :titlesonly:

   intro
   specification/*
   installation/*

   development/*
   
   todo
   changelog

.. automodule:: models

.. autoclass::
     :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
