"""_____________________________________________________________________

:PROJECT: AnIML_python

*animl - .*

:details: AnIML.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200407

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"


import os
import unittest
import logging
import animllib.animl_creator as ac

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

class TestAniMLCreator(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        
        self.ad = ac.AnIMLCreator()
        self.ad.add_sample()


    def testCompile(self):
        animl_doc = self.ad.compile()

        self.assertIn("AnIML", animl_doc)
        self.assertIn("core:1.0", animl_doc)

        print(self.ad)
