# AnIML_python - a AnIML python library

This is a python library to process Analytical Information Markup Language ( [AnIML](https://www.animl.org/)) documents, see also [https://www.animl.org/](https://www.animl.org/).


## Key Features (planned)

* AnIML document creation
* AnIML document parsing
* AnIML document validation
* AnIML example document generation for testing and simulation purposes 

## Installation

The easiest installation can be done via pip3 (from [PiPy](http://pypi.org)):

    pip install --user animallib  # --user option installs animl_python for current user only

alternatively one could use the setup.py script, like:

    cd [animl_python  folder containing setup.py]
    pip3 install --user -r requirements.txt .
    
    # or 
    python3 setup.py install

    # development installation
    pip3 install -e -r requirements.txt .


## Testing the library with unittests

    cd [animl_python folder containing setup.py]
    python3 setup.py test


## Repository Maintainer:

* mark doerr (mark.doerr@uni-greifswald.de)
